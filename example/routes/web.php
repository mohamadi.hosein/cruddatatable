<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/create-table',[\App\Http\Controllers\TableController::class,'operate']);
Route::get('/remove-table',[\App\Http\Controllers\TableController::class,'removeTable']);


Route::get('/',[\App\Http\Controllers\EmployeeController::class,'index'])->name('tbl.index');

Route::post('tabledit/action',[\App\Http\Controllers\EmployeeController::class,'action'])->name('tabledit.action');




