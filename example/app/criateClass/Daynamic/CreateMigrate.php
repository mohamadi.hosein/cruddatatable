<?php
namespace App\criateClass\Daynamic;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

class CreateMigrate
{
    private $table_name,$fileds;
    public function __construct($table_name,$fileds=null)
    {
        $this->table_name=$table_name;
        $this->fileds=$fileds;
    }

    public function createTable(){
        $table_name=$this->table_name;
        $fileds=$this->fileds;

        if(!Schema::hasTable($table_name)){
            Schema::create($table_name,function (Blueprint $table) use($fileds,$table_name){
                $table->increments('id');
                if(count($fileds)>0){
                    foreach ($fileds as $filed){
                        $table->{$filed['type']}($filed['name'],$filed['size']);
                    }
                }
                $table->timestamps();
            });
            Artisan::call('make:model', ['name' => $table_name]);
            Artisan::call('make:controller', ['name' => $table_name.'Controller']);
            Artisan::call('db:seed');

            return response()->json(['message'=>'جدول شما با موفقیت ایجاد شد'],200);
        }
        return response()->json(['message'=>'جدول از قبل موجود میباشد'],400);
    }


    public function dropTable(){
        Schema::dropIfExists($this->table_name);
        return $this;
    }

}
