<?php

namespace App\Http\Controllers;

use App\criateClass\Daynamic\CreateMigrate;

class TableController extends Controller
{
    public function operate(){
        $createTable=new CreateMigrate(Config('initdata.table_name'), Config('initdata.fileds'));
        return $createTable->createTable();
    }
    public function removeTable(){
        $table_name='Employee';
        $createTable=new CreateMigrate($table_name);
        $createTable->dropTable();
        return response()->json(['message'=>'جدول شما با موفقیت حذف شد'],200);
    }
}
