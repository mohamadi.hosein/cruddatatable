<?php

namespace App\Http\Controllers;

use App\criateClass\Repositories\EmployeeRepository;

use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    protected $employeeRepository;

    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Exception
     * @description show data for view page
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {

            $data = Employee::query();

            $keyserch = $request->get('keyserch');
            $valueSerch = $request->get('valueSerch');

            if ($keyserch && $valueSerch) {
                $data->where($keyserch, 'like', "%$valueSerch%");
            }

            $posts = $data->select('id', 'companyName', 'mobile', 'city', 'description', 'status');

            return datatables()->of($posts)
                ->make(true);
        }

        $tag = $this->filterSerch();
        $pageLength=Employee::$pageLength;

        return view('welcome', compact('tag','pageLength'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @description Edit or delete with action parameter from resive front
     */
    function action(Request $request)
    {
        if ($request->ajax()) {
            if ($request->action == 'delete')
                Employee::where('id', $request->id)->delete();

                $data = [
                    'companyName' => $request->companyName,
                    'mobile' => $request->mobile,
                    'city' => $request->city,
                    'description' => $request->description,
                    'status' => $request->status,
                ];
                Employee::where('id', $request->id)->update($data);

            return response()->json($request);
        }
    }

    private function filterSerch()
    {
      return  collect(Config("initdata.fileds"))->filter(function ($item) {
            return ($item['searchable'] == true);
        })->pluck('name')->toArray();
    }

}
