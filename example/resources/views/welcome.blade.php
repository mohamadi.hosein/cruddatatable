<html dir="rtl">
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.8/css/fixedHeader.dataTables.min.css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://markcell.github.io/jquery-tabledit/assets/js/tabledit.min.js"></script>
    <link
        href="https://nightly.datatables.net/buttons/css/buttons.dataTables.css?_=c6b24f8a56e04fcee6105a02f4027462.css"
        rel="stylesheet" type="text/css"/>
    <script
        src="https://nightly.datatables.net/buttons/js/buttons.html5.js?_=c6b24f8a56e04fcee6105a02f4027462"></script>

    <script
        src="https://nightly.datatables.net/buttons/js/dataTables.buttons.js?_=c6b24f8a56e04fcee6105a02f4027462"></script>
    <script
        src="https://nightly.datatables.net/buttons/js/buttons.colVis.js?_=c6b24f8a56e04fcee6105a02f4027462"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.8/js/dataTables.fixedHeader.min.js"></script>

</head>
<body>
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title ">نمایش اطلاعات کارمند</h3>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <div class="col-lg-6">
                    <div class="input-group">
                        <select id="listsearch" class="form-control" style="width:40%">
                            <option value=""></option
                            @foreach($tag as $key=>$value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                        <input type="text" name="value" id="value" class="form-control" style="width:60%"
                               placeholder="Enter Filter Value">
                        <span class="input-group-btn">
                            <button class="btn btn-secondary" id="customesearch"><i
                                    class="glyphicon glyphicon-search"></i></button>
                            </span>
                    </div>
                </div>
                @csrf
                <table id="editable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        @foreach(Config("initdata.fileds") as $value)
                            <th style="width:{{$value['width']}}">{{$value['header']}}</th>
                        @endforeach
                        <th style="width:5%">action</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<script type="text/javascript">
    $(document).ready(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $("input[name=_token]").val()
            }
        });

        //
        var table = $('#editable').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            bAutoWidth: false,
            pageLength: {{$pageLength}},

            ajax: {
                url: "{{ route('tbl.index') }}",
                data: function (d) {
                    d.keyserch = $('#listsearch option:selected').text(),
                        d.valueSerch = $('#value').val()
                }
            },
            dom: 'Bfrtip',
            columnDefs: [
                {
                    targets: [0],
                    className: 'noVis'
                }

            ],
            buttons: [
                {
                    extend: 'collection',
                    text: 'Show columns',
                    className: 'col',
                    buttons: []
                }],
            columns: [
                {data: "id", name: "id", sortable: false},
                    @foreach(Config("initdata.fileds") as $key=>$value)
                {
                    data: "{{$value['name']}}", name: "{{$value['name']}}", sortable:{{$value['order']?1:0}}== 1
        },
        @endforeach

    ]

    });

        //request for update and delete
        $('#editable').on('draw.dt', function () {
            $('#editable').Tabledit({
                url: "{{ route('tabledit.action') }}",
                dataType: 'json',
                columns: {
                    identifier: [0, 'id'],
                    editable: [[1, 'companyName'], [2, 'mobile'], [3, 'city'], [4, 'description'], [5, 'status', '{"1":"active", "0":"Deactive"}']]
                },
                restoreButton: false,
                onSuccess: function (data, textStatus, jqXHR) {
                    if (data.action == 'delete') {
                        $('#' + data.id).remove();
                        $('#sample_data').DataTable().ajax.reload();
                    }
                }
            });
        });

        //show columen in button select
        table.columns().every(function (index) {
            table.button().add('0-' + index, {
                action: function (e, dt, button, config) {
                    table.column(index).visible(table.column(index).visible() ? false : true);
                    $(button).toggleClass("hidden");
                },
                text: $(table.column(index).header()).text(),
                enabled: $(table.column(index).header()).hasClass('noVis') ? false : true
            });
        });

        //after search reload datatable
        $('#customesearch').click(function () {
            $('#editable').DataTable().draw(true);
        })


    });
</script>
