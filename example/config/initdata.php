<?php

return [

    'table_name' => 'Employee',
    'fileds' => [
        ['name' => 'companyName', 'type' => 'string', 'size' => 30, 'width' => '16%','header'=>'نام کمپانی','searchable'=>true,'order'=>true],
        ['name' => 'mobile', 'type' => 'string', 'size' =>20, 'width' => '5%','header'=>'تلفن همراه','searchable'=>false,'order'=>true],
        ['name' => 'city', 'type' => 'string', 'size' => 25, 'width' => '5%','header'=>'نام شهر','searchable'=>true,'order'=>true],
        ['name' => 'description', 'type' => 'string', 'size' => 250, 'width' => '60%','header'=>'توضیحات','searchable'=>false,'order'=>true],
        ['name' => 'status', 'type' => 'tinyInteger', 'size' => null, 'width' => '5%','header'=>' وضعیت','searchable'=>true,'order'=>false],
    ],
];

